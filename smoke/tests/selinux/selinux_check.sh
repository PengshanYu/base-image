#!/bin/bash

set -uo pipefail

STATUS=$(sestatus | awk -F ':' '/SELinux status/ { gsub(" ",""); print $2}')
CURRENT_MODE=$(sestatus | awk -F ':' '/Current mode/ { gsub(" ",""); print $2}')
MODE_FROM_CFG=$(sestatus | awk -F ':' '/Mode from config file/ { gsub(" ",""); print $2}')

# Check selinux status
if [ "$STATUS" != "enabled" ]; then
   echo "FAIL : Selinux is ${STATUS}, it is not enabled on this system!"
   echo "FAIL : $(basename "$0" .sh)"
   exit 1
else
   echo "PASS : Selinux is ${STATUS} on this system."

   # Compared current mode and mode from config file
   if [ "$CURRENT_MODE" == "$MODE_FROM_CFG" ]; then
      echo "INFO : Selinux current mode is the same as the mode from config file."

      if [ "$CURRENT_MODE" == "enforcing" ] && [ "$MODE_FROM_CFG" == "enforcing" ]; then
         echo "PASS : Both of the modes are Enforcing."
         echo "PASS : $(basename "$0" .sh)"
         exit 0
      else
         echo "INFO : Selinux current mode is: ${CURRENT_MODE}."
         echo "INFO : The mode from config file is: ${MODE_FROM_CFG}."
         echo "FAIL : Either current mode or config file mode is NOT set to enforcing"
         echo "FAIL: $(basename "$0" .sh)"
         exit 1
      fi
   else
      echo "INFO : Selinux current mode is: ${CURRENT_MODE}."
      echo "INFO : The mode from config file is: ${MODE_FROM_CFG}."
      echo "FAIL : These modes are different!"
      echo "FAIL: $(basename "$0" .sh)"
      exit 1
   fi
fi
