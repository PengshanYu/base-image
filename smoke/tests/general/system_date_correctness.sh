#!/bin/bash

set -uxo pipefail

if ! rpm -q chrony; then
   echo "FAIL: chrony is not installed"
   exit 1
fi

if [ "$(systemctl is-enabled chronyd)" != "enabled" ] || [ "$(systemctl is-active chronyd)" != "active" ]; then
   echo "FAIL: chronyd service isn't enabled and active, please check it!"
   exit 1
fi

# Are we synchronized?
STATE=$(timedatectl show | grep NTPSynchronized | cut -d= -f2)
if [ "$STATE" != "yes" ]; then
   echo "FAIL: NTP not synchronized!"
   exit 1
fi

ERRORS=$(journalctl -u chronyd.service | grep -E 'error|fail')
if [ -n "$ERRORS" ]; then
   echo "FAIL: There are errors or failures in chrony log!"
   echo "$ERRORS"
   exit 1
fi

echo "PASS: System is NTP synchronized"
