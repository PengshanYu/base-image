Smoke test for OSBuild_variables.

## This Test Suite includes these tests:

1. Add regression test for rpm-ostree bug, related to the OSBuild variable that is not being converted properly at building time.
    
2. The test adds a repo, that use the dnf variables (like '$baseach' or '$releasever').

3. Checks if the bug still exists by running "rpm-ostree refresh-md" command and reports if the original error: "error: Updating rpm-md repo 'osbuild': cannot update repo 'osbuild': Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried; Last error: Status code: 404" is detected on Ostree system.
