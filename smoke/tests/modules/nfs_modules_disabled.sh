#!/bin/bash

# List of NFS-related kernel modules
nfs_modules=("nfs" "nfs_layout_nfsv41_files" "nfsv3" "nfsv4" "nfs_layout_flexfiles" "nfs_acl" "nfsd" "blocklayoutdriver")

# Flag to track if any module is loaded
nfs_loaded=0

echo "Checking if NFS modules are disabled..."

for module in "${nfs_modules[@]}"; do

    # attempt loading the module first
    sudo modprobe "$module" 2>/dev/null

    if lsmod | grep -q "^$module"; then
        echo "FAIL: NFS module '$module' is loaded."
        nfs_loaded=1
    fi
done

if [ "$nfs_loaded" -eq 0 ]; then
    echo "PASS: All NFS modules are disabled."
    exit 0
else
    echo "FAIL: Some NFS modules are still loaded."
    exit 1
fi