#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running server container in $BAD_CONTAINER with mount file volume"
# shellcheck disable=SC2086
podman exec -d $BAD_CONTAINER bash -c "cd /var ; if [ -e $tst_name ]; then rm -f $tst_name; fi ; podman run $CONTAINER_PARAMS -v hello:/var/run/ipc --replace --name $cntr_mount_server $BASE_CONTAINER_IMAGE /var/tst_socket_fd --test-name $tst_name --file-name /var/$f_name --mount" > /dev/null

printf "%s\n" "-- Running client container in $BAD_CONTAINER with mount file volume"
# shellcheck disable=SC2086
podman exec -d $BAD_CONTAINER bash -c "cd /var ; podman run $CONTAINER_PARAMS -v hello:/var/run/ipc --replace --ipc container:$cntr_mount_server --name $cntr_mount_client $BASE_CONTAINER_IMAGE /var/tst_socket_fd --test-name $tst_name --file-name /var/$f_name --mount" > /dev/null