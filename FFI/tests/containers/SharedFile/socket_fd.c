// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright Red Hat
 * Author: Dennis Brendel <dbrendel@redhat.com>
 *         weiwang <weiwang@redhat.com>
 */

#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/un.h>
#include <poll.h>
#include <stdlib.h>
#include <getopt.h>

enum {
    EXIT_FAILURE_TIMEOUT = EXIT_FAILURE + 1,
    EXIT_FAILURE_SENDING_FD,
    EXIT_FAILURE_RECEIVING_FD,
    EXIT_FAILURE_READING_FD,
    EXIT_FAILURE_TEST_WRITE_ACCESS,
    EXIT_FAILURE_INVALID_PARAMS,
    EXIT_FAILURE_CREATE_SOCKET,
    EXIT_FAILURE_CONNECT_TO_SOCKET,
    EXIT_FAILURE_FILE_OPEN,
    EXIT_FAILURE_LISTEN_TO_SOCKET,
    EXIT_FAILURE_SOCKET_ACCEPT,
};

#define BUFFSIZE 256

char *test_name;
int exit_status_timeout = EXIT_FAILURE_TIMEOUT;  // Default timeout exit status
int mount = 0;  // Default value indicating unmounted volume

void usage(const char *prog_name) {
    fprintf(stderr, "Usage: %s --test-name <name> --file-name <path> [--mount] [--timeout-expected]\n", prog_name);
}

struct option long_options[] = {
    {"test-name", required_argument, 0, 't'},
    {"file-name", required_argument, 0, 'f'},
    {"mount", no_argument, &mount, 1},  // Set mount to 1 if --mount is present
    {"timeout-expected", no_argument, &exit_status_timeout, 0}, // Set exit_status_timeout to 0 if --timeout-expected is present
    {0, 0, 0, 0}
};

// Send fd to socket
int send_fd(int socket, int fd)
{   
    struct msghdr msg = {0};
    char buf[CMSG_SPACE(sizeof(fd))];
    memset(buf, '\0', sizeof(buf));
    char iobuf[1];
    struct iovec io = {
        .iov_base = iobuf,
        .iov_len = sizeof(iobuf)
    };

    msg.msg_iov = &io;
    msg.msg_iovlen = 1;
    msg.msg_control = buf;
    msg.msg_controllen = sizeof(buf);

    struct cmsghdr * cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(fd));

    *((int *) CMSG_DATA(cmsg)) = fd;
    msg.msg_controllen = CMSG_SPACE(sizeof(fd));

    if (sendmsg(socket, &msg, 0) < 0)
    {
        printf("FAIL:%s: Send file descriptor failed as %s", test_name, strerror(errno));
        return EXIT_FAILURE_SENDING_FD;
    }
    return 0;
}

// Receive fd from socket
int receive_fd(int socket)
{   
    struct msghdr msg = {0};

    char m_buf[BUFFSIZE];
    struct iovec io = {
        .iov_base = m_buf,
        .iov_len = sizeof(m_buf)
    };

    msg.msg_iov = &io;
    msg.msg_iovlen = 1;

    char c_buf[BUFFSIZE];
    msg.msg_control = c_buf;
    msg.msg_controllen = sizeof(c_buf);

    if (recvmsg(socket, &msg, 0) < 0)
    {
        printf("FAIL:%s: Receive file descriptor failed as %s \n", test_name, strerror(errno));
        return EXIT_FAILURE_RECEIVING_FD;
    }

    struct cmsghdr * cmsg = CMSG_FIRSTHDR(&msg);
    unsigned char * data = CMSG_DATA(cmsg);
    int fd = *((int *) data);
    return fd;
}

/*
If the file is unmounted, the file transferred via the socket is not readable. 
If the file is mounted, it is readable but not writable.
*/ 
// expected_result :
//           Non-zero ---> Read allowed
//           zero ---> Read not allowed
int test_read_access(int fd, int expected_result)
{
    printf("- - - Test read access - - -\n");
    char buf_read[BUFFSIZE] = { 0 };
    if (expected_result)
    {
        // Read allowed
        // If read() returns a value is no more than 0, it means that no file data has been read,
    	// return EXIT_FAILURE_READING_FD.
        if (read(fd, buf_read, sizeof(buf_read)) <= 0)
        {
            printf("FAIL:%s: Read mount volume is ALLOWED!!!\n", test_name);
            return EXIT_FAILURE_READING_FD;
        }
        return 0;
    }
    else
    {
        // Read not allowed
        // If the read() return value is greater than 0, it means that the file data has been read,
    	// return EXIT_FAILURE_READING_FD.
        if (read(fd, buf_read, sizeof(buf_read)) > 0)
        {
            printf("FAIL:%s: Read unmounted volume is FORBIDDEN!!!\n", test_name);
            return EXIT_FAILURE_READING_FD;
        }
        printf("PASS:%s: Read unmounted volume should fail\n", test_name);
        close(fd);
        return 0;
    }
}


// If the file is mounted, it isn't writable to a read-only file.
int test_write_access(int fd, int expected_result)
{
    if (!test_read_access(fd, expected_result))
    {
        printf("- - - Test write access - - -\n");
        char buf_write[BUFFSIZE] = { 0 };
        memset(buf_write, 1, sizeof(buf_write));

        // Write not allowed
        // If the write() return value is greater than 0, it means that the file data has been written,
        // return EXIT_FAILURE_TEST_WRITE_ACCESS.
        if (write(fd, buf_write, sizeof(buf_write)) > 0)
        {
                printf("FAIL:%s: Write to read-only file is FORBIDDEN!!!\n", test_name);
                return EXIT_FAILURE_TEST_WRITE_ACCESS;
        }

        printf("PASS:%s: Write to read-only file should fail\n", test_name);
        close(fd);
        return 0;
    }
    return EXIT_FAILURE_TEST_WRITE_ACCESS;
}

int main(int argc, char * argv[]) {
    int s, ss, timeout=5;
    unsigned int t;
    struct sockaddr_un client, server;
    char *file_name = NULL;
    int opt;

    while ((opt = getopt_long(argc, argv, "t:f:", long_options, NULL)) != -1) {
        switch (opt) {
            case 't':
                test_name = optarg;
                break;
            case 'f':
                file_name = optarg;
                break;
            case 0: // Handles `no_argument` flags like --mount and --timeout-expected
                break;
            default:
                usage(argv[0]);
                return EXIT_FAILURE_INVALID_PARAMS;
        }
    }

    if (!test_name || !file_name) {
        usage(argv[0]);
        return EXIT_FAILURE_INVALID_PARAMS;
    }

    int fd = open(file_name, O_RDONLY);
    if (fd < 0)
    {
        printf("FAIL:%s: Open file %s in read-only access mode failed as %s\n", test_name, file_name, strerror(errno));
        return EXIT_FAILURE_INVALID_PARAMS;
    }

    s = socket(AF_UNIX, SOCK_STREAM, 0);
    if (s < 0)
    {
        printf("FAIL:%s: Create socket failed as %s! \n", test_name, strerror(errno));
        return EXIT_FAILURE_CREATE_SOCKET;
    }

    client.sun_family = AF_UNIX;
    strcpy(client.sun_path, test_name);
    if (bind(s, (struct sockaddr *)&client, sizeof(client)) == -1) 
    {
        printf(" - - - Client start - - -\n");

        if (connect (s, (struct sockaddr *)&client, sizeof(client)) == -1)
        {
            printf("FAIL:%s: Connect to an opened socket failed as %s!\n", test_name, strerror(errno));
            return EXIT_FAILURE_CONNECT_TO_SOCKET;
        } 
        else 
        {
            printf("- - - Send file - - -\n");
            int fd = open(file_name, O_RDONLY);
            if (fd < 0)
            {
                printf("FAIL:%s: Open file %s in read-only access mode failed as %s\n", test_name, file_name, strerror(errno));
                return EXIT_FAILURE_FILE_OPEN;
            }
             
            send_fd(s, fd);
            close(fd);
            close(s);
        }
    }
    else
    {
        printf("- - - Server start - - -\n");
        if (listen(s, 5) == -1)
        {
            printf("FAIL:%s: Listen to the socket connections failed as %s\n", test_name, strerror(errno));
            return EXIT_FAILURE_LISTEN_TO_SOCKET;
        }

        struct pollfd fds;
        fds.fd = s;
        fds.events = POLLIN;
        if (poll(&fds, 1, timeout * 1000) == 0) {
           fprintf(stderr, "Timeout!\n");
           close(s);
           return exit_status_timeout;
        }

        t = sizeof(server);
        if ((ss = accept(s, (struct sockaddr *)&server, &t)) == -1) {
            printf("FAIL:%s: Accept the socket connections failed as %s\n", test_name, strerror(errno));
            return EXIT_FAILURE_SOCKET_ACCEPT;
        }
        int fd = receive_fd(ss);
        close(ss);

        // mount:
        //       zero --- unmount volume
        //       Non-zero --- mount volume
        if (mount == 0)
        {
            test_read_access(fd, mount);
        }
        else    
        {
           test_write_access(fd, mount);
        }
    }
} 
