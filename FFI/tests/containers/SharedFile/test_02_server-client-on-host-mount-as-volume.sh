#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running server on host with mount file volume"
./tst_asilb_socket_fd --test-name $tst_name --file-name $f_name --mount &

printf "%s\n" "-- Running client on host with mount file volume"
./tst_asilb_socket_fd --test-name $tst_name --file-name $f_name --mount > /dev/null
