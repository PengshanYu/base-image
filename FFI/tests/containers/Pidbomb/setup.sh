#!/bin/bash

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

# setting up the container
# the container is set up in common.sh
test_image=$(build_container_image gcc)
cntr_builder="builder"
file="tst_pidbomb"

# compile pidbomb.c inside the builder container
if ! test -x "$file"; then
    echo "Preparing test for containers"
    run_test_container_from_image "$cntr_builder" "$test_image"
    echo "Compile '$file' in '$cntr_builder'"
    podman exec -it "$cntr_builder" gcc -o "$file" -lrt pidbomb.c -Wall -Werror
fi

# copying files
copy_script_to_container $file
echo "Copying '$file' file"
