#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running system_v_semaphore in server container in $BAD_CONTAINER."
# shellcheck disable=SC2154
podman exec -d $BAD_CONTAINER bash -c "podman run $CONTAINER_PARAMS --replace --name server $BASE_CONTAINER_IMAGE $(get_path_to_script "$file")" > /dev/null

printf "%s\n" "-- Running system_v_semaphore in client container using the same namespace as a server in $BAD_CONTAINER. Expected: success."
podman exec -d $BAD_CONTAINER bash -c "podman run $CONTAINER_PARAMS --replace --name client --ipc container:server $BASE_CONTAINER_IMAGE $(get_path_to_script "$file")" > /dev/null